use keys::Keys64;
use key::{Key, KeyCode, Layer};

#[repr(C)]
#[derive(Debug, PartialEq, Default)]
pub struct KeyReport {
    pub modifiers: u8,
    pub reserved: u8,
    pub keys: [u8; 6],
}

pub struct Decoder<'a> {
    base_layer: &'a Layer,
}

impl <'a> Decoder<'a> {
    pub const fn new(base_layer: &'a Layer) -> Self {
        Decoder { base_layer }
    }

    pub fn decode(&self, keys: Keys64) -> Option<KeyReport> {
        let layer = find_active_layer(self.base_layer, keys);
        let mut report_modifiers = 0;
        let mut report_keys = [0; 6];
        {
            let mut report_keys_iter = report_keys.iter_mut();
            let mut add_key = |key: KeyCode| {
                match key {
                    KeyCode::Keyboard(code) => {
                        if let Some(k) = report_keys_iter.next() {
                            *k = code
                        }
                    }
                    KeyCode::Modifier(code) => {
                        report_modifiers |= code;
                    }
                    KeyCode::Media(_code) => {
                        // TODO
                    }
                }
            };
            for key in keys.on() {
                match layer.key(key as usize) {
                    &Key::KeyCode(k) => {
                        add_key(k);
                    }
                    &Key::KeyCodes2(k1, k2) => {
                        add_key(k1);
                        add_key(k2);
                    }
                    &Key::KeyCodes3(k1, k2, k3) => {
                        add_key(k1);
                        add_key(k2);
                        add_key(k3);
                    }
                    &Key::Blank => {}
                    &Key::Layer(_) => {}
                }
            }
        }
        Some(KeyReport {
            modifiers: report_modifiers,
            keys: report_keys,
            reserved: 0,
        })
    }
}

fn get_pressed_layer_key(layer: &Layer, keys: Keys64) -> Option<&Layer> {
    for key in keys.on() {
        match layer.key(key as usize) {
            &Key::Layer(layer) => {
                return Some(layer);
            }
            _ => {}
        }
    }
    None
}

fn find_active_layer(base_layer: &Layer, keys: Keys64) -> &Layer {
    let mut active_layer = base_layer;
    while let Some(layer) = get_pressed_layer_key(active_layer, keys) {
        active_layer = layer;
    }
    active_layer
}

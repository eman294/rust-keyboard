use core::cmp::PartialEq;
use core::ptr;
use core::fmt;

#[derive(Debug, PartialEq)]
pub enum Key {
    Blank,
    KeyCode(KeyCode),
    KeyCodes2(KeyCode, KeyCode),
    KeyCodes3(KeyCode, KeyCode, KeyCode),
    //KeyCodes([Option<KeyCode>; 4]),
    Layer(&'static Layer),
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum KeyCode {
    Modifier(u8),
    Keyboard(u8),
    Media(u16),
}

pub const COLS: usize = 12;
pub const ROWS: usize = 4;

pub struct Layer(pub [Key; COLS * ROWS]);

impl PartialEq for Layer {
    fn eq(&self, other: &Layer) -> bool {
        ptr::eq(self, other)
    }
}
               
impl fmt::Debug for Layer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let ptr: *const Layer = self;
        write!(f, "Layer {:?}", ptr)
    }
}

impl Layer {
    pub fn key(&self, index: usize) -> &Key {
        &self.0[index]
    }
}

//impl <T: Copy> Layer<T> {
    //pub fn decode<'a>(&'a self, keys: &'a Keys) -> impl Iterator<Item=T> + 'a {
        //keys.on().map(move |key_idx| {
            //self.0[key_idx as usize] 
        //})
    //}
//}

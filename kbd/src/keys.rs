use bit_field::BitField;
use byteorder::{ByteOrder, LittleEndian as LE};

pub type Index = u8;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Keys32(pub u32);

impl Keys32 {
    /// All keys off
    #[inline]
    pub fn none() -> Self {
        Keys32(0)
    }

    /// Total number of keys
    #[inline]
    pub fn len() -> Index {
        24
        //(mem::size_of::<Self>() * 8) as u8
    }

    #[inline]
    pub fn read(buf: &[u8]) -> Self {
        Keys32(LE::read_u32(buf))
    }

    #[inline]
    pub fn write(&self, buf: &mut [u8]) {
        LE::write_u32(buf, self.0);
    }

    /// Get the state of a key
    #[inline]
    pub fn key(&self, index: Index) -> bool {
        debug_assert!(index < Self::len());
        self.0.get_bit(index)
    }

    /// Set the state of a key
    #[inline]
    pub fn set_key(&mut self, index: Index, state: bool) {
        debug_assert!(index < Self::len());
        self.0.set_bit(index, state);
    }

    /// Iterator of the indexes of pressed keys
    #[inline]
    pub fn on<'a>(&'a self) -> impl Iterator<Item = Index> + 'a {
        (0..Self::len()).filter(move |&i| self.key(i))
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Keys64(pub u64);

impl Keys64 {
    /// Total number of keys
    pub const fn len() -> Index {
        48
    }

    /// All keys off
    pub fn none() -> Self {
        Keys64(0)
    }

    pub fn from_halves(left: Keys32, right: Keys32) -> Self {
        let six = 6;
        let six_bits = 0b111111;
        Keys64(
              (((left.0  & (six_bits << (six * 0))) as u64) << (six as u64 * 0)) // 0
            | (((right.0 & (six_bits << (six * 0))) as u64) << (six as u64 * 1)) // 1
            | (((left.0  & (six_bits << (six * 1))) as u64) << (six as u64 * 1)) // 2
            | (((right.0 & (six_bits << (six * 1))) as u64) << (six as u64 * 2)) // 3
            | (((left.0  & (six_bits << (six * 2))) as u64) << (six as u64 * 2)) // 4
            | (((right.0 & (six_bits << (six * 2))) as u64) << (six as u64 * 3)) // 5
            | (((left.0  & (six_bits << (six * 3))) as u64) << (six as u64 * 3)) // 6
            | (((right.0 & (six_bits << (six * 3))) as u64) << (six as u64 * 4)) // 7
        )
    }

    /// Get the state of a key
    #[inline]
    pub fn key(&self, index: Index) -> bool {
        debug_assert!(index < Self::len());
        self.0.get_bit(index)
    }

    /// Set the state of a key
    #[inline]
    pub fn set_key(&mut self, index: Index, state: bool) {
        debug_assert!(index < Self::len());
        self.0.set_bit(index, state);
    }

    /// Iterator of the indexes of pressed keys
    #[inline]
    pub fn on<'a>(&'a self) -> impl Iterator<Item = Index> + 'a {
        (0..Self::len()).filter(move |&i| self.key(i))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn len() {
        assert_eq!(Keys32::len(), 24);
    }

    #[test]
    fn none() {
        let keys = Keys32::none();
        for i in 0..Keys32::len() {
            assert_eq!(keys.key(i), false);
        }
        assert_eq!(keys.on().count(), 0);
    }

    #[test]
    fn read_write() {
        let mut keys = Keys32::none();
        let on = [0u8, 2, 5, 19, 23];
        for &i in on.iter() {
            keys.set_key(i, true);
        }
        let mut buf = [0u8; 4];
        keys.write(&mut buf);
        let keys = Keys32::read(&buf);
        for i in 0..Keys32::len() {
            let should_be_on = on.iter().any(|&j| j == i);
            assert_eq!(keys.key(i), should_be_on);
        }
        assert_eq!(keys.on().count(), on.len());
        for (actual, &expected) in keys.on().zip(on.iter()) {
            assert_eq!(actual, expected);
        }
    }

    #[quickcheck]
    fn keys64_key(index: u8) -> bool {
        if index >= Keys64::len() {
            return true;
        }
        let keys = Keys64::none();
        keys.key(index) == false
    }

    #[test]
    fn keys64_none() {
        let keys = Keys64::none();
        assert_eq!(keys.on().count(), 0);
    }

    #[test]
    fn keys64_on() {
        let keys = Keys64::from_halves(Keys32(0b0_0000100_100010), Keys32(0));
        let mut on = keys.on();
        assert_eq!(on.next(), Some(1));
        assert_eq!(on.next(), Some(5));
        assert_eq!(on.next(), Some(14));
        assert_eq!(on.next(), None);
    }
}

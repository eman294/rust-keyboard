#[macro_export]
macro_rules! __keyboard_layer_key {
    ([]) => {
        Key::Blank
    };
    ([$k1:path]) => {
        Key::KeyCode($k1)
    };
    ([$k1:path | $k2:path]) => {
        Key::KeyCodes2($k1, $k2)
    };
    ([$k1:path | $k2:path | $k3:path]) => {
        Key::KeyCodes3($k1, $k2, $k3)
    };
    ([layer $k:expr]) => {
        Key::Layer($k)
    };
}

#[macro_export]
macro_rules! keyboard_layer {
    (
        $(
            [
                $(
                    $key:tt
                ),+
            ],
        )+
    ) => {
        {
            use ::key::Key;
            [
                $(
                    $(
                        __keyboard_layer_key!($key)
                    ),+
                    ,
                )+
            ]
        }
    };
}

#[cfg(test)]
mod tests {
    use keycodes::*;
    use key::Key;

    #[test]
    fn test() {
        let layer = keyboard_layer![
            [[Q], [ ], [E | LEFT_SHIFT]],
        ];
        let expected = [
            Key::KeyCode(Q),
            Key::Blank,
            Key::KeyCodes2(E, LEFT_SHIFT),
        ];
        assert_eq!(&layer[..], &expected[..]);
    }
}

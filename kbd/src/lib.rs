#![cfg_attr(not(feature = "std"), no_std)]
#![feature(conservative_impl_trait)]
#![feature(const_fn)]
#![cfg_attr(test, feature(plugin))]
#![cfg_attr(test, plugin(quickcheck_macros))]

#[cfg(feature = "std")]
extern crate core;
extern crate wiring;
extern crate bit_field;
extern crate byteorder;
#[cfg(test)]
extern crate quickcheck;

pub mod keys;
pub mod matrix;
pub mod decoder;
pub mod key;
pub mod keycodes;
#[macro_use]
pub mod layer_macros;
pub mod qwerty;

